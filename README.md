# README #

### What is this repository for? ###
This repo consists of my work in the Advanced Course in Web Programming I am attending currently. (Feb-May 2018)

### How do I get set up? ###

We have various assignments in this course including HTML/CSS coding, python and some various essay questions.
Everything will be pushed to this repository as I complete the assignments.

Deadline for this course is 31.5

### Who do I talk to? ###

Contact me @ gallen.victor@gmail.com if you have any questions!
Happy coding :)